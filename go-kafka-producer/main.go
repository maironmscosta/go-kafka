package main

import (
	"bitbucket.org/maironmscosta/go-kafka/util"
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/segmentio/kafka-go"
	"log"
	"time"
)

func main() {

	//_, err := kafka.DialLeader(context.Background(), "tcp", util.Address, util.TopicA, util.Partition)
	//if err != nil {
	//	log.Fatal("failed to dial leader:", err)
	//}

	w := &kafka.Writer{
		Addr:     kafka.TCP(util.Address),
		Topic:    util.TopicA,
		Balancer: &kafka.LeastBytes{},
	}

	defer w.Close()

	key01 := "mairon@email.com"
	key02 := "marilda@email.com"
	key03 := "debora@email.com"
	testes := []map[string]struct {
		Email     string
		Nome      string
		UUID      string
		Partition int
	}{
		{
			key01: {
				Nome:      "Mairon Costa",
				Email:     key01,
				Partition: 0,
			},
			key02: {
				Nome:      "Marilda Firmino",
				Email:     key02,
				Partition: 1,
			},
			key03: {
				Nome:      "Debora Maia",
				Email:     key03,
				Partition: 2,
			},
		},
	}

	//doneProducer := make(chan string)

	for {

		for _, m := range testes {

			for key, value := range m {

				//go func(doneProducer chan string) {

				sprintf := fmt.Sprintf("vai enviar ao topico: %s - key: %s - hora: %v", util.TopicA, key, time.Now().Format(time.RFC3339))
				fmt.Println(sprintf)

				value.UUID = uuid.New().String()

				marshal, err := json.Marshal(value)
				if err != nil {
					log.Fatal("failed to dial leader:", err)
				}

				err = w.WriteMessages(context.Background(),
					kafka.Message{
						Key:       []byte(key),
						Value:     marshal,
						Partition: value.Partition,
					},
				)

				if err != nil {
					log.Fatal("failed to write messages:", err)
				}

				//}(doneProducer)
			}

		}

		//time.Sleep(2 * time.Second)
	}

	//fmt.Printf("doneProducer: %s ", <- doneProducer)

	if err := w.Close(); err != nil {
		log.Fatal("failed to close writer:", err)
	}

}
