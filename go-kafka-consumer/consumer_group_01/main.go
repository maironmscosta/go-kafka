package main

import (
	"bitbucket.org/maironmscosta/go-kafka/util"
	"context"
	"fmt"
	"github.com/segmentio/kafka-go"
	"log"
	"time"
)

func main() {

	ListTopics()

	// make a new reader that consumes from topic-A, partition 0, at offset 42
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{util.Address},
		Topic:   util.TopicA,
		//Partition: util.Partition,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
		//StartOffset: kafka.FirstOffset,
		GroupID: "consumer_group_01",
	})

	//r.SetOffset(2)
	ctx := context.Background()
	for {

		m, err := r.ReadMessage(ctx)
		if err != nil {
			log.Fatal("failed to consume message:", err)
			//continue
		}
		fmt.Printf("message at offset %d: %s = %s - partition: %d \n",
			m.Offset, string(m.Key), string(m.Value), m.Partition)

		time.Sleep(1 * time.Second)

	}

	if err := r.Close(); err != nil {
		log.Fatal("failed to close reader:", err)
	}

}

func ListTopics() {

	conn, err := kafka.Dial("tcp", util.Address)
	if err != nil {
		panic(err)
	}

	defer conn.Close()

	partitions, err := conn.ReadPartitions()
	if err != nil {
		panic(err.Error())
	}

	m := map[string]struct{}{}
	for _, p := range partitions {
		m[p.Topic] = struct{}{}
	}

	for k := range m {
		fmt.Println(k)
	}

}
