# Material de apoio

## Kafka com docker

* https://www.baeldung.com/ops/kafka-docker-setup 

## Kafka Go

* https://github.com/segmentio/kafka-go

## Observações

para fazer downgrade e o docker funcionar, basta rodar o seguinte comando:

* ``` sudo dnf downgrade containerd.io ```

